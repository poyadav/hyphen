#!/bin/bash
############################################################################
#Developed for hyphen 
#This script is used to check sanity for hyphen package   
#Usage : ./make_check.sh
#
############################################################################

file_count=13
pkg="hyphen"

check_return_value () {
    if [ $1 != 0 ] ; then
        exit $1
    fi
}

check_files_count(){
    no_of_files=`rpm -qlv $pkg | wc -l`
    if [ $no_of_files == $file_count ]
    then
        echo "PASS"
    else
        echo "FAIL"
    fi
}

cd ../source
autoreconf -i
automake --foreign -Wall
./configure && make && make check
check_files_count
